class Tide(object):

    def __init__(self):
        self._tide_diff = ""
        self._date = None
        self._tides = []
        self._location = ""

    @property
    def tide_diff(self):
        return self._tide_diff
    @tide_diff.setter
    def tide_diff(self, val):
        self._tide_diff = val

    @property
    def date(self):
        return self._date
    @date.setter
    def date(self, val):
        self._date = val

    @property
    def tides(self):
        return self._tides
    @tides.setter
    def tides(self, val):
        self._tides = val

    @property
    def location(self):
        return self._location
    @location.setter
    def location(self, val):
        self._location = val

    def from_dict(d):
        self._tide_diff = d['tide_diff']

    def to_json(self):
        return {
            'location': self._location,
            'date': self._date,
            'tide_diff': self._tide_diff,
            'tides': self._tides
        }
