data_src_id = 'F-A0021-001'
data_src_uri = 'https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/{}?Authorization={}&format={}'

favorate_spot = ['金山', '頭城', '貢寮']


query_all_tides_by_date = """
SELECT
    daily.tide_info_id,
    info.location,
    daily.id AS daily_tide_id,
    daily.tide_diff,
    daily.tide_date,
    detail.tide_status,
    detail.tide_height,
    detail.tide_ts
FROM tides_info info
LEFT JOIN daily_tides daily ON info.id = daily.tide_info_id
LEFT JOIN tides_detail detail ON daily.id = detail.daily_tide_id
WHERE daily.tide_date = '{}';
"""
