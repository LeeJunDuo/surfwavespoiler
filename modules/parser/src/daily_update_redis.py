import sys
from os import getenv
from datetime import datetime, timedelta
from collections import defaultdict
import json

import redis

from db_client import DBClient
from crawler_src import query_all_tides_by_date

gen_redis_key = lambda a, b: '{}_{}'.format(a, b)
redis_cli = redis.Redis(host=getenv('REDIS_HOST'), port=getenv('REDIS_PORT'), db=getenv('REDIS_DB'))

def main():
    today = _today()
    
    # delete data by yesterday
    yesterday = today - timedelta(days = 1)
    _clear_tide()
    # _delete_tide_by_date(yesterday)

    # generate data by today
    all_tides_record = _retrieve_all_tides_by_date(today)

    d = defaultdict(list)
    tide_diff = 3
    for record in all_tides_record:
        d[_to_redis_key(record)].append((record[tide_diff], record))
    for (redis_key, redis_src) in d.items():
        redis_val = _record_to_json_str(redis_src)

        # key '{location}_{date}'
        redis_cli.set(redis_key, redis_val)

def _clear_tide():
    deprecate_keys = redis_cli.keys('*')
    for key in deprecate_keys:
        print('delete key {}'.format(key))
        redis_cli.delete(key)

def _delete_tide_by_date(d):
    tide_keys = tuple(redis_cli.keys(gen_redis_key('*', d)))
    for key in tide_keys:
        redis_cli.delete(key)

def _record_to_json_str(redis_src):
    tides_of_1day = []
    for tide in redis_src:
        (tide_diff, tide_detail) = tide
        print(tide_detail)
        tides_of_1day.append({
            'status': tide_detail[5],
            'height': tide_detail[6],
            'ts': tide_detail[7].strftime('%H:%M')
        })
    return json.dumps({
        'tide_diff': tide_diff,
        'tides': tides_of_1day
    })

def _to_redis_key(record):
    location = record[1]
    tide_date = record[4]
    return gen_redis_key(location, tide_date)


def _retrieve_all_tides_by_date(d):
    sql = query_all_tides_by_date.format(d.strftime('%Y-%m-%d'))
    return DBClient.select(sql)

def _today():
    return datetime.now()

if __name__ == '__main__':
    sys.exit(main())

