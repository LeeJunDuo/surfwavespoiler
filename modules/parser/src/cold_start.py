from os import getenv
import sys
from collections import namedtuple, defaultdict
from datetime import datetime

from urllib import request
import json


from crawler_src import data_src_uri, data_src_id, favorate_spot
from tide import Tide
from db_client import DBClient
import daily_update_redis

tides_detail_data = []
def main():
    DBClient.exec("TRUNCATE TABLE tides_detail, daily_tides, tides_info;")

    ACCESS_TOKEN = getenv('ACCESS_TOKEN')
    url = data_src_uri.format(data_src_id, ACCESS_TOKEN, 'JSON')
    req = request.Request(url, {}, method = 'GET')
    resp = request.urlopen(req)
    data = json.loads(resp.read())

    # mock data
    # data = _get_mock_data()

    cwb_open_data = data['cwbopendata']
    dataset = cwb_open_data['dataset']
    locations = dataset['location']

    favorate_locations = retrieve_favorate_location(locations)

    tide_info_1month = []
    for location in favorate_locations:
        lo_name = location['locationName']
        for raw_tide in location['time']:
            tide_info_1month.append(_parse_tide_info(lo_name, raw_tide))

    # print(tide_info_1month)
    d = defaultdict(list)
    for spot_tide in tide_info_1month:
        d[spot_tide.location].append(spot_tide)
    insert_db(d)

    daily_update_redis.main()

def insert_db(d):
    spots = list(d.keys())
    DBClient.exec(_create_tide_info_sql(spots))
    DBClient.exec(_create_daily_tides_sql(d, spots))
    DBClient.exec(_create_tides_detail_sql())


def _create_tide_info_sql(spots):
    sql_template = 'INSERT INTO tides_info (location) values {};'
    insert_value = ','.join(list(map(lambda s: "('{}')".format(s), spots)))
    sql = sql_template.format(insert_value)
    return sql

def _create_daily_tides_sql(d, spots):
    tides_info_records = DBClient.select('SELECT * FROM tides_info;')
    tides_info = {
        r[1]: r[0] for r in tides_info_records
    }

    daily_tides_src = []
    daily_tide_tuple = namedtuple('daily_tide', ['tides_info_id', 'tide_dt', 'tide_diff'])
    global tides_detail_data
    for spot in spots:
        tides_info_id = tides_info[spot]
        tides_in_month = d[spot]
        for tide in tides_in_month:
            daily_tides_src.append(daily_tide_tuple._make((tides_info_id, tide.date, tide.tide_diff)))
            tides_detail_data.append({
                'tides_info_id': tides_info_id,
                'tides_detail': tide.tides,
                'tide_dt': datetime.fromisoformat(tide.date)
            })

    insert_value = ','.join(list(map(lambda d: "('{}', '{}', '{}')".format(d.tides_info_id, d.tide_dt, d.tide_diff), daily_tides_src)))
    sql_template = 'INSERT INTO daily_tides (tide_info_id, tide_date, tide_diff) values {};'
    sql = sql_template.format(insert_value)
    return sql

def _create_tides_detail_sql():
    gen_key = lambda tides_info_id, tide_date: '{}_{}'.format(tides_info_id, tide_date.strftime('%Y-%m-%d') if isinstance(tide_date, datetime) else tide_date)
    tides_info_daily_records = DBClient.select('\
        SELECT \
            d.id,\
            info.id as tides_info_id,\
            d.tide_date\
        FROM daily_tides d\
        LEFT JOIN tides_info info ON info.id = d.tide_info_id;')
    
    lookup_table = {
       gen_key(r[1], r[2]) : r[0] for r in tides_info_daily_records
    }
    print(lookup_table)
    print(tides_detail_data[0])
    detail_tuple = namedtuple('tides_detail', ['daily_tide_id', 'tide_status', 'tide_ts', 'tide_height'])
    tides_detail_src = []
    for detail in tides_detail_data:
        daily_tide_id = lookup_table[gen_key(detail['tides_info_id'], detail['tide_dt'])]
        for tide in detail['tides_detail']:
            tides_detail_src.append(detail_tuple._make((daily_tide_id, tide.tide, datetime.fromisoformat(tide.dt), tide.height)))

    insert_value = ','.join(list(map(lambda s: "('{}', '{}', '{}', '{}')".format(s.daily_tide_id, s.tide_ts.strftime('%H:%M'), s.tide_status, s.tide_height), tides_detail_src)))
    sql_template = 'INSERT INTO tides_detail (daily_tide_id, tide_ts, tide_status, tide_height) values {};'
    sql = sql_template.format(insert_value)
    print(sql)
    return sql


def _parse_tide_info(lo_name, raw_tide):
    tide = Tide()
    tide.location = lo_name
    tide.date = raw_tide['validTime']['startTime']
    (tide_diff, tides) = _parse_single_tide(raw_tide['weatherElement'])
    tide.tide_diff = tide_diff
    tide.tides = tides
    return tide

def _parse_single_tide(we):
    return (_retrieve_tide_value(we[1]), _retrieve_tide_height(we[2]))

def _retrieve_tide_value(obj):
    return obj['value']

def _retrieve_tide_height(obj):
    attr = ['tide', 'dt', 'height']
    tide_tuple = namedtuple('tide_tuple', attr)
    data = []
    for t in obj['time']:
        if isinstance(t, dict):
            we = t['weatherElement']
            data.append([we[0]['value'], we[1]['value'], we[2]['elementValue']['value']])
    data_tuple = list(map(tide_tuple._make, data))
    return data_tuple

def retrieve_favorate_location(locations):
    temp = [filter_by_spot(spot, lo) for spot in favorate_spot for lo in locations]
    return list(filter(lambda d: d is not None, temp))

def filter_by_spot(spot, lo):
    return lo if spot in lo['locationName'] else None

def _get_mock_data():
    with open('mock_data.txt', 'r') as raw_data:
        data = json.load(raw_data)
    return data

if __name__ == '__main__':
    sys.exit(main())

