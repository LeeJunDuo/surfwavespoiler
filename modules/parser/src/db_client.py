from os import getenv

import psycopg2

class DBClient(object):
    conn = psycopg2.connect(
            database=getenv('POSTGRES_DB'),
            user=getenv('POSTGRES_USER'),
            password=getenv('POSTGRES_PASSWORD'),
            host=getenv('POSTGRES_HOST'), port=getenv('POSTGRES_PORT'))

    cursor = conn.cursor()

    @classmethod
    def select(cls, sql):
        cls.cursor.execute(sql)
        return cls.cursor.fetchall()

    @classmethod
    def exec(cls, sql):
        cls.cursor.execute(sql)
        cls.conn.commit()

