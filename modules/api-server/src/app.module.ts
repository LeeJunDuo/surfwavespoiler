import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TidesModule } from './tides/tides.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'mysecretpassword',
      database: 'jun',
      entities: [join(__dirname, '**/entities/*.entity{.ts,.js}')],
      synchronize: false,
    }),
    TidesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

