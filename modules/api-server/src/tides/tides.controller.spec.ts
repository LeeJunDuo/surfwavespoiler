import { Test, TestingModule } from '@nestjs/testing';
import { TidesController } from './tides.controller';
import { TidesService } from './services/tides/tides.service';
import { TidesInfo } from './entities/tides_info.entity';
import { TidesInfoDto } from './dto/tides_info.dto';

import { response, request } from 'express';

let app: TestingModule;
let controller: TidesController;
let spyService: TidesService;

describe('Tides Controller', () => {

  beforeEach(async () => {
    app = await Test.createTestingModule({
      controllers: [TidesController],
      providers: [
        {
          provide: TidesService,
          useFactory: () => ({
            findTidesInfo: jest.fn(() => true),
            create: jest.fn(() => true),
            patchTidesInfo: jest.fn(() => true),
            deleteTidesInfo: jest.fn(() => true),
          }),
        }],
    }).compile();

    controller = app.get<TidesController>(TidesController);
    spyService = app.get(TidesService);
  });

  it('tidesController should be defined', () => {
    expect(controller).toBeDefined();
  });
});

describe('findAllLocations', () => {
  it('shoud invoke tideService.findAllLocations', async () => {
    controller.findTidesInfo(request);
    expect(spyService.findTidesInfo).toHaveBeenCalledTimes(1);
  });
});

describe('create location', () => {
  it('controllers should call tidesServic.create', async () => {
    const d: TidesInfoDto = {
      location: undefined,
    };
    controller.createTidesInfo(request, response);
    expect(spyService.create).toHaveBeenCalledWith(d);
  });


  it('controllers should call tidesServic.patchTidesInfo', async () => {
    const d: TidesInfoDto = {
      location: undefined,
    };
    controller.patchTidesInfo(request, response);
    expect(spyService.patchTidesInfo).toHaveBeenCalledWith(d);
  });


  it('controllers should call tidesServic.deleteTidesInfo', async () => {
    const d: TidesInfoDto = {
      location: '磯崎',
    };
    controller.deleteTidesInfo(request, response);
    expect(spyService.deleteTidesInfo).toHaveBeenCalledWith(d);
  });
});
