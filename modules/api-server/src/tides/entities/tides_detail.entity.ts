import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { IsOptional, IsNumber, IsDefined } from 'class-validator';
import { CrudValidationGroups } from "@nestjsx/crud";
import { DailyTides } from "./daily_tides.entity";

const { CREATE, UPDATE } = CrudValidationGroups;
@Entity()
export class TidesDetail {
    @IsOptional({ always: true })
    @PrimaryGeneratedColumn()
    id: number;


    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNumber()
    @Column({
        name: 'daily_tide_id',
        type: 'integer',
    })
    dailyTideId: number;

    @IsDefined({ groups: [CREATE, UPDATE] })
    @Column({
        name: 'tide_ts',
        type: 'time',
    })
    tideTs: Date;

    @IsDefined({ groups: [CREATE, UPDATE] })
    @Column({
        name: 'tide_status',
        type: 'varchar',
    })
    tideStatus: string;

    @IsDefined({ groups: [CREATE, UPDATE] })
    @Column({
        name: 'tide_height',
        type: 'integer',
    })
    tideHeight: number;

    @ManyToOne(type => DailyTides, dailyTides => dailyTides.tidesDetail)
    dailyTides: DailyTides[];
}
