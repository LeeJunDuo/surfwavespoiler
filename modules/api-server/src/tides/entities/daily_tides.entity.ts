import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { TidesInfo } from "./tides_info.entity";
import { TidesDetail } from "./tides_detail.entity";

@Entity()
export class DailyTides {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => TidesInfo, tidesInfo => tidesInfo.dailyTides)
    tidesInfo: TidesInfo;

    @OneToMany(type => TidesDetail, tidesDetail => tidesDetail.dailyTides)
    tidesDetail: TidesDetail[];
}
