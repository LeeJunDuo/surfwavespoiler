import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { DailyTides } from './daily_tides.entity';

@Entity()
export class TidesInfo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 32 })
    location: string;

    @OneToMany(type => DailyTides, dailyTides => dailyTides.tidesInfo)
    dailyTides: DailyTides[];
}
