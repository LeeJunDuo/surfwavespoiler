import { Module } from '@nestjs/common';
import { TidesController } from './tides.controller';
import { RedisService } from './services/redis/redis.service';
import { TidesService } from './services/tides/tides.service';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { TidesInfo } from './entities/tides_info.entity';
import { PortgresCrudService } from './services/portgres-crud/portgres-crud.service';
import { TidesDetail } from './entities/tides_detail.entity';


@Module({
  imports: [
    TypeOrmModule.forFeature([TidesInfo, TidesDetail]),
    /** other orm modules like redis */
  ],
  providers: [
    TidesService,
    RedisService,
    PortgresCrudService],
  controllers: [TidesController],
  exports: [
    TidesService,
    RedisService,
    PortgresCrudService],
})
export class TidesModule { }
