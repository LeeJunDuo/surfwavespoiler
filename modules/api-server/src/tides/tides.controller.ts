import { Get, Patch, Controller, Post, Req, Body, Res, HttpStatus, Put, Param, Delete } from '@nestjs/common';
import { Request, Response } from 'express';

import { TidesService } from './services/tides/tides.service';
import { TidesInfo } from './entities/tides_info.entity';
import { TidesInfoDto } from './dto/tides_info.dto';

@Controller('surftidespoiler')
export class TidesController {
    constructor(private readonly tidesService: TidesService) { }

    @Get('tidesInfo')
    findTidesInfo(@Req() req: Request): Promise<TidesInfo[]> {
        const name = req.param('location', undefined);
        return this.tidesService.findTidesInfo(name);
    }


    @Post('tidesInfo')
    async createTidesInfo(
        @Req() req: Request,
        @Res() res: Response): Promise<Response> {
        res.header('Content-Type', 'application/json');
        const tidesInfo: TidesInfoDto = req.body;
        let execStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        let msg;
        let result: TidesInfo;
        try {
            result = await this.tidesService.create(tidesInfo);
            execStatus = HttpStatus.CREATED;
            msg = `Entity ${result.location} (id ${result.id}) created`;
        } catch (err) {
            console.error(err);
            msg = 'Insert error';
        }

        return res.status(execStatus).send({ message: msg });
    }

    @Patch('tidesInfo/:id')
    async patchTidesInfo(
        @Req() req: Request,
        @Res() res: Response): Promise<Response> {
        const id: number = +req.params.id;
        const tidesInfo: TidesInfoDto = req.body;
        let execStatus = HttpStatus.NOT_FOUND;
        let msg;
        let result: TidesInfo;
        try {
            result = await this.tidesService.patchTidesInfo(id, tidesInfo);
            execStatus = HttpStatus.OK;
            msg = `Entity ${result.location} (id ${result.id}) patched`;
        } catch (err) {
            console.error(err);
            msg = `TidesInfo with id ${id} is NOT exist`;
        }

        return res.status(execStatus).send({ message: msg });
    }

    @Delete('tidesInfo/:id')
    async deleteTidesInfo(
        @Req() req: Request,
        @Res() res: Response): Promise<Response> {
        const id: number = +req.params.id;
        let execStatus = HttpStatus.NOT_FOUND;
        let msg;
        let result: TidesInfo;

        try {
            result = await this.tidesService.deleteTidesInfo(id);
            execStatus = HttpStatus.OK;
            msg = `Entity ${result.location} (id ${result.id}) deleted`;
        } catch (err) {
            console.error(err);
            msg = `TidesInfo wit id ${id} is NOT exist`;
        }

        return res.status(execStatus).send({ message: msg });
    }
}
