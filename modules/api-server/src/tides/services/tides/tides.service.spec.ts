import { Test, TestingModule } from '@nestjs/testing';
import { TidesService } from './tides.service';
import { TidesInfoDto } from 'src/tides/dto/tides_info.dto';

class MockTidesService {
  findTidesInfo() {
    return [
      {
        id: 57,
        location: '金山區',
      }];
  }
  create() {
    return {
      id: 100,
      location: '佳樂水',
    };
  }
}

let service: TidesService;
describe('TidesService', () => {
  beforeEach(async () => {
    const TidesServcieProvider = {
      provide: TidesService,
      useClass: MockTidesService,
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [TidesService, TidesServcieProvider],
    }).compile();

    service = module.get<TidesService>(TidesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});


describe('findTidesInfo', () => {
  it('should more than one record', () => {
    const result = service.findTidesInfo(undefined);
    expect(result).toBeDefined();
  });

  it('should be "金山區" and just one record only ', () => {
    const location = '金山區';
    const result = service.findTidesInfo(location);
    expect(result).toEqual(new MockTidesService().findTidesInfo());
  });
});


describe('create tidesInfo', () => {
  it('should create one record', () => {
    const tidesInfo: TidesInfoDto = {
      location: '佳樂水',
    };
    const result = service.create(tidesInfo);
    expect(result).toEqual(new MockTidesService().create());
  });

  it('should be max lenght excpetion on location ', () => {
    const tidesInfo: TidesInfoDto = {
      location: '新竹市竹圍123123123123123123123123123123123123123123123',
    };
    const result = service.create(tidesInfo);
    expect(result).toEqual(new MockTidesService().create());
  });
});
