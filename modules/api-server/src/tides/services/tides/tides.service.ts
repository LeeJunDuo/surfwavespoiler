import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RedisService } from '../redis/redis.service';
import { TidesInfo } from '../../entities/tides_info.entity';
import { TidesInfoDto } from 'src/tides/dto/tides_info.dto';


@Injectable()
export class TidesService {
    constructor(
        @InjectRepository(TidesInfo)
        private readonly locationRepository: Repository<TidesInfo>, ) { }

    async findTidesInfo(name: string): Promise<TidesInfo[]> {
        let condition: any;
        if (name !== undefined) {
            condition = {
                where: [{ location: name }],
            };
        }
        return await this.locationRepository.find(condition);
    }

    async create(dto: TidesInfoDto): Promise<TidesInfo> {
        const entity = new TidesInfo();
        entity.location = dto.location;
        return await this.locationRepository.save(entity);
    }

    async patchTidesInfo(id: number, dto: TidesInfoDto): Promise<TidesInfo> {
        let entities: TidesInfo[];
        let target: TidesInfo;
        entities = await this.locationRepository.findByIds([id]);
        target = entities[0];
        if (target === undefined) {
            throw new NotFoundException();
        }
        target.location = dto.location;
        return await this.locationRepository.save(target);
    }

    async deleteTidesInfo(id: number): Promise<TidesInfo> {
        let entities: TidesInfo[];
        let target: TidesInfo;

        entities = await this.locationRepository.findByIds([id]);
        target = entities[0];

        if (target === undefined) {
            throw new NotFoundException();
        }
        return await this.locationRepository.remove(target);
    }
}
