import { Test, TestingModule } from '@nestjs/testing';
import { PortgresCrudService } from './portgres-crud.service';

class MockPostgresCrudService {
}

describe('PortgresCrudService', () => {
  let app: TestingModule;
  let service: PortgresCrudService;

  beforeEach(async () => {
    const PostgresCrudServiceProvider = {
      provide: PortgresCrudService,
      useClass: MockPostgresCrudService,
    };

    app = await Test.createTestingModule({
      providers: [PortgresCrudService, PostgresCrudServiceProvider],
    }).compile();

    service = app.get<PortgresCrudService>(PortgresCrudService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
