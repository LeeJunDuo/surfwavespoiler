import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { TidesDetail } from '../../entities/tides_detail.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PortgresCrudService extends TypeOrmCrudService<TidesDetail> {
    constructor(@InjectRepository(TidesDetail) repo) {
        super(repo);
    }

}
