import { Test, TestingModule } from '@nestjs/testing';
import { TidesModule } from './../src/tides/tides.module';
import * as request from 'supertest';
import { TidesInfoDto } from 'src/tides/dto/tides_info.dto';
import { TidesInfo } from 'src/tides/entities/tides_info.entity';

describe('TidesController (e2e)', () => {
    let app;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [TidesModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/surftidespoiler/tidesInfo (POST)', () => {
        const tidesInfo: TidesInfoDto = {
            location: '磯崎',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .post('/surftidespoiler/tidesInfo')
            .expect(200);
    });

    it('/surftidespoiler/tidesInfo (POST)', () => {
        const tidesInfo: TidesInfoDto = {
            location: '金山',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .post('/surftidespoiler/tidesInfo')
            .expect(200);
    });

    it('/surftidespoiler/tidesInfo (GET)', () => {
        const tidesInfo_1: TidesInfoDto = {
            location: '磯崎',
        };
        const tidesInfo_2: TidesInfoDto = {
            location: '金山',
        };
        const expectResult = [tidesInfo_1, tidesInfo_2];
        return request(app.getHttpServer())
            .get('/surftidespoiler/tidesInfo')
            .expect(200)
            .expect(expectResult);
    });

    it('/surftidespoiler/tidesInfo?location=金山 (GET)', () => {
        const tidesInfo: TidesInfoDto = {
            location: '金山',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .get('/surftidespoiler/tidesInfo?location=金山')
            .expect(200)
            .expect(expectResult);
    });

    it('/surftidespoiler/tidesInfo (POST) duplicate unique key', () => {
        const tidesInfo: TidesInfoDto = {
            location: '磯崎',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .post('/surftidespoiler/tidesInfo')
            .expect(500);
    });

    it('/surftidespoiler/tidesInfo/:id (Patch) resource not found', () => {
        const tidesInfo: TidesInfoDto = {
            location: '花蓮磯崎',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .post('/surftidespoiler/tidesInfo/1000')
            .expect(404);
    });

    it('/surftidespoiler/tidesInfo/:id (Patch) ', () => {
        const tidesInfo: TidesInfoDto = {
            location: '花蓮磯崎',
        };
        const expectResult = [tidesInfo];
        return request(app.getHttpServer())
            .post('/surftidespoiler/tidesInfo/1')
            .expect(200);
    });

    it('/surftidespoiler/tidesInfo?location=花蓮磯崎 (GET)', () => {
        const tidesInfoDto: TidesInfoDto = {
            location: '花蓮磯崎',
        };
        const tidesInfo: TidesInfo = {
            id: 1,
            location: '花蓮磯崎',
            dailyTides: [],
        };
        const expectResult = [tidesInfoDto];
        return request(app.getHttpServer())
            .get('/surftidespoiler/tidesInfo?location=花蓮磯崎')
            .expect(200)
            .expect(tidesInfo);
    });

    it('/surftidespoiler/tidesInfo/1000 (DELETE) resource not found', () => {
        const tidesInfoDto: TidesInfoDto = {
            location: '花蓮磯崎',
        };
        const expectResult = [tidesInfoDto];
        return request(app.getHttpServer())
            .delete('/surftidespoiler/tidesInfo/1000')
            .expect(404);
    });

    it('/surftidespoiler/tidesInfo/1 (DELETE)', () => {
        const tidesInfoDto: TidesInfoDto = {
            location: '花蓮磯崎',
        };
        const expectResult = [tidesInfoDto];
        return request(app.getHttpServer())
            .delete('/surftidespoiler/tidesInfo/1')
            .expect(200);
    });

    it('/surftidespoiler/tidesInfo?location=花蓮磯崎 (GET)', () => {
        return request(app.getHttpServer())
            .get('/surftidespoiler/tidesInfo?location=花蓮磯崎')
            .expect(200)
            .expect([]);
    });
});
