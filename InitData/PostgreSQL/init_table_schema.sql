USE master;
GO

CREATE DATABASE
IF NOT EXISTS jun;

USE jun;
GO

CREATE TABLE
IF NOT EXISTS tides_info
(
    id serial PRIMARY KEY,
    location VARCHAR
(50) UNIQUE NOT NULL
);

CREATE TABLE
IF NOT EXISTS daily_tides
(
    id serial PRIMARY KEY,
    tide_info_id integer REFERENCES tides_info
(id),
    tide_diff VARCHAR
(10) NOT NULL,
    tide_date DATE NOT NULL
);

CREATE TABLE
IF NOT EXISTS tides_detail
(
    id serial PRIMARY KEY,
    daily_tide_id integer REFERENCES daily_tides
(id),
    tide_ts TIME NOT NULL, 
    tide_status VARCHAR
(10) NOT NULL,
    tide_height integer NOT NULL
);




